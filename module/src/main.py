import uvicorn

from fastapi_versioning import VersionedFastAPI, version

from fastapi import FastAPI
from google.cloud import bigquery, storage

from constants import PROJECT_ID
from connector import Connector
from manager import Manager
from bigquery_helper import BigQueryHelper
from storage_helper import StorageHelper
from service import Service


app = FastAPI(title="Toulouse Meteo Data Connector")

bq_writer = BigQueryHelper(bigquery.Client(project=PROJECT_ID))
storage_helper = StorageHelper(storage.Client(project=PROJECT_ID))

connector = Connector()
service = Service(connector=connector)
manager = Manager(service=service, bq_writer=bq_writer, storage_helper=storage_helper)


@app.post("/all/export")
@version(1)
def extract_all():
    return manager.extract_all()


@app.post("/all/recent")
@version(1)
def extract_new():
    return manager.extract_new()


@app.post("/{station}/export")
@version(1)
def extract_all(station):
    return manager.extract_all(station)


@app.post("/{station}/recent")
@version(1)
def extract_new(station):
    return manager.extract_new(station)


app = VersionedFastAPI(app, version_format="{major}", prefix_format="/v{major}")

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)
